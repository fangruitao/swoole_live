<?php
/**
 * redis相关配置
 * Created by PhpStorm.
 * User: baidu
 * Date: 18/3/23
 * Time: 上午9:17
 */

return [
    'host' => '127.0.0.1',
    'port' => 6379,
    'out_time' => 120, //验证码失效时间
    'timeOut' => 500, // 连接超时时间
    'logout_time' => 0, // 登录的失效时间， 0-永久
    'live_game_key' => 'live_game_key'  //存放websocket的fd id
];