# swolle 适配 thinkphp5.1
## 修改thinkphp\library\think\Request.php
### 1.重新获取页面url
修改 public function pathinfo() 这个方法
注释 if (is_null($this->pathinfo)) { 与 }
修改 public function path() 这个方法
注释 if (is_null($this->path)) { 与 }

### 2.获取参数
#```
 public function __call($method, $args)
    {
        //当$this->hook 为空直接返回参数
        if(empty($this->hook)){
           return $args;
        }
        if (array_key_exists($method, $this->hook)) {
            array_unshift($args, $this);
            return call_user_func_array($this->hook[$method], $args);
        } else {
            throw new Exception('method not exists:' . static::class . '->' . $method);
        }
    }
#```
