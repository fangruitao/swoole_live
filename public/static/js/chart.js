//var wsUrl = "ws://www.s_live.com:8811";
var wsUrl = "ws://www.s_live.com:8812";

var websocket = new WebSocket(wsUrl);

//实例对象的onopen属性
websocket.onopen = function(evt) {
    console.log("conected-swoole-success");
}

// 实例化 onmessage
websocket.onmessage = function(evt) {
    pushChart(evt.data);
    //console.log("ws-server-return-char-data:" + evt.data);
}

//onclose
websocket.onclose = function(evt) {
    console.log("close");
}
//onerror

websocket.onerror = function(evt, e) {
    console.log("error:" + evt.data);
}

function pushChart(data) {
    data = JSON.parse(data);
    if(data.pushType != 'chart'){
        return;
    }
    html = '<div class="comment">';
    html += '<span>'+data.user+'</span>';
    html += '<span>'+data.content+'</span>';
    html += '</div>';
    $('#comments').append(html);
}