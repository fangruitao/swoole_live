<?php
namespace app\index\controller;
use app\common\lib\ali\Sms;
use app\common\lib\Util;
use app\common\lib\Redis;
class Send
{
    /**
     * 发送验证码
     */
    public function index() {
        // tp  input
        //$phoneNum = request()->get('phone_num', 0, 'intval');
        $phoneNum = isset($_GET) && !empty($_GET['phone_num']) ? intval($_GET['phone_num']) : '';
        if(empty($phoneNum)) {
            // status 0 1  message data
            return Util::show(config('code.error'), '手机号码不能为空');
        }
        //判断手机号码是否合法
        if(!preg_match("/^1[34578]{1}\d{9}$/",$phoneNum)){
            return Util::show(config('code.error'), '手机号码不合法');
        }
        //tood
        // 生成一个随机数
        $code = rand(1000, 9999);

        $taskData = [
            'method' => 'sendSms',
            'data' => [
                'phone' => $phoneNum,
                'code' => $code,
            ]
        ];
        //投放发送验证码任务
        $_POST['http_server']->task($taskData);
        return Util::show(config('code.success'), '发送成功!');
        //下面的短信可以跑通
        $redis = new \Swoole\Coroutine\Redis();
        $redis->connect(config('redis.host'), config('redis.port'));
        $redis->set(Redis::smsKey($phoneNum), $code, config('redis.out_time'));
        return Util::show(config('code.success'), '发送成功!');

        try {
            $response = Sms::sendSms($phoneNum,$code);
        }catch (\Exception $e) {
            // todo
            return Util::show(config('code.error'), '阿里大于内部异常');
        }
        if($response->Code === "OK") {
            // 异步redis
            $redis = new \Swoole\Coroutine\Redis();
            $redis->connect(config('redis.host'), config('redis.port'));
            $redis->set(Redis::smsKey($phoneNum), $code, config('redis.out_time'));

            return Util::show(config('code.success'), '发送成功!');
        } else {
            return $response->Code;
            return Util::show(config('code.error'), '验证码发送失败');
        }


    }
}
