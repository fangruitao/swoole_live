<?php
namespace app\index\controller;
use app\common\lib\Util;
class Chart
{
    public function index()
    {
        // 登录
        if(empty($_POST['game_id'])) {
            return  Util::show(config('code.error'), '没有比赛');
        }
        if(empty($_POST['content'])) {
            return Util::show(config('code.error'), '没有输入内容');
        }

        $data = [
            'user' => "用户".rand(0, 2000),
            'content' => $_POST['content'],
        ];
        $taskData = [
            'method' => 'pushChar',
            'data' => $data
        ];
        // 将推送的工作投放到task
        $_POST['http_server']->task($taskData);

        return Util::show(config('code.success'), '发送成功!', $data);
    }


}
