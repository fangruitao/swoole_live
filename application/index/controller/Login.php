<?php
namespace app\index\controller;

use app\common\lib\Util;
use app\common\lib\Redis;
use app\common\lib\redis\Predis;
class Login
{
    public function index() {
        // phone code
        $phoneNum = isset($_GET['phone_num']) && !empty($_GET['phone_num']) ? intval($_GET['phone_num']) : '';
        $code = isset($_GET['code']) && !empty($_GET['code']) ? intval($_GET['code']) : '';
        if(empty($phoneNum) || empty($code)) {
            return Util::show(config('code.error'), '手机号码或验证码不能为空!');
        }

        // redis code
        try {
            $redisCode = Predis::getInstance()->get(Redis::smsKey($phoneNum));
        }catch (\Exception $e) {
            echo $e->getMessage();return;
        }
        if($redisCode == $code) {
            // 写入redis
            $data = [
                'user' => $phoneNum,
                'srcKey' => md5(Redis::userkey($phoneNum)),
                'time' => time(),
                'isLogin' => true,
            ];
            Predis::getInstance()->set(Redis::userkey($phoneNum), $data, config('redis.logout_time'));

            return Util::show(config('code.success'), '登录成功', $data);
        } else {
            return Util::show(config('code.error'), '登录失败');
        }
        // redis.so
    }
}
