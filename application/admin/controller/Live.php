<?php
namespace app\admin\controller;
use app\common\lib\Util;
use app\common\lib\redis\Predis;
class Live
{

    public function push() {

        if(!isset($_GET) || empty($_GET)) {
            return Util::show(config('code.error'), '数据不能为空!');
        }
        //从mysql表获取
        $teams = [
            1 => [
                'name' => '马刺',
                'logo' => '/imgs/team1.png',
            ],
            4 => [
                'name' => '火箭',
                'logo' => '/imgs/team2.png',
            ],
        ];

        $data = [
            'type' => intval($_GET['type']),
            'title' => !empty($teams[$_GET['team_id']]['name']) ?$teams[$_GET['team_id']]['name'] : '直播员',
            'logo' => !empty($teams[$_GET['team_id']]['logo']) ?$teams[$_GET['team_id']]['logo'] : '',
            'content' => !empty($_GET['content']) ? $_GET['content'] : '',
            'image' => !empty($_GET['image']) ? $_GET['image'] : '',
        ];
        // 获取连接的用户
        // 赛况的基本信息入库   2、数据组织好 push到直播页面
        $taskData = [
            'method' => 'pushLive',
            'data' => $data
        ];
        // 将推送的工作投放到task
        $_POST['http_server']->task($taskData);
        //推送给所有websocket的客户端
        //第一种使用swoole自带获取客户端id(包括tcp的客户端，需要判断是否websocket客户端)
     /*   try {
            foreach ($_POST['http_server']->connections as $fd) {
                //判断是否websocket连接
                if($_POST['http_server']->isEstablished($fd)){
                    $_POST['http_server']->push($fd, json_encode($data));
                }
            }
        } catch (\Exception $e){
            echo $e->getMessage();
        }*/
        //第二种使用redis，在onOpen中记录id，在onClose删除id
//        $fds = Predis::getInstance()->sMembers(config('redis.live_game_key'));
//        foreach ($fds as $fd) {
//            $_POST['http_server']->push($fd, json_encode($data));
//        }

        return Util::show(config('code.success'), '添加成功!');

    }

}
