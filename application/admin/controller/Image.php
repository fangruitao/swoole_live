<?php
namespace app\admin\controller;
use app\common\lib\Util;

class Image
{
    /**
     * tp上传图片
     */
    public function index() {
        if(!isset($_FILES) || empty($_FILES)){
            return Util::show(config('code.error'), '没有选择图片!');
        }
        $file = request()->file('file'); //文件控件的name
        $info = $file->move('../public/static/upload/');//入口是在server的服务下
        if($info) {
            $data = [
                'image' => config('live.host')."/upload/".$info->getSaveName(),
            ];
            return Util::show(config('code.success'), '上传成功!', $data);
        }else {
            return Util::show(config('code.error'), '上传失败!');
        }
    }
    /**
     * 原生上传图片
     */
    public function index1() {
        if(!isset($_FILES) || empty($_FILES)){
            return Util::show(config('code.error'), '没有选择图片!');
        }
//        $file = request()->file('file');
        $file_name = time().rand(10000,9999).'.jpg'; //生成的文件名
        $dir = APP_PATH . '../public/static/upload/'.date('Ymd').'/'; //目录
        //判断目录是否存在
        if(!file_exists($dir)){
            @mkdir($dir, 0777, true); //修改权限，以及递归创建目录(前提要创建的目录的父目录有写权限)
        }
        try {
            $info = move_uploaded_file($_FILES["file"]["tmp_name"], $dir.$file_name);
        } catch (\Exception $e){
            echo $e->getMessage();
        }
        if($info) {
            $data = [
                'image' => config('live.host')."/upload/".$file_name,
            ];
            return Util::show(config('code.success'), '上传成功!', $data);
        }else {
            return Util::show(config('code.error'), '上传失败!');
        }
    }
}
