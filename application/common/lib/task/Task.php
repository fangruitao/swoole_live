<?php
/**
 * 代表的是  swoole里面 后续 所有  task异步 任务 都放这里来
 * Date: 18/3/27
 * Time: 上午1:20
 */
namespace app\common\lib\task;
use app\common\lib\ali\Sms;
use app\common\lib\redis\Predis;
use app\common\lib\Redis;
class Task{

    /**
     * 异步发送 验证码
     * @param $data
     * @param $serv swoole server对象
     */
    public function sendSms($data, $serv='') {
        try {
            $response = Sms::sendSms($data['phone'], $data['code']);
        }catch (\Exception $e) {
            // todo
            return false;
        }

        // 如果发送成功 把验证码记录到redis里面
        if($response->Code === "OK") {
            //同步redis
            Predis::getInstance()->set(Redis::smsKey($data['phone']), $data['code'], config('redis.out_time'));
        }else {
            return false;
        }
        return true;
    }

    /**
     * 通过task机制发送赛况实时数据给客户端
     * @param $data
     * @param $serv swoole server对象
     */
    public function pushLive($data, $serv) {
        $clients = Predis::getInstance()->sMembers(config("redis.live_game_key"));
        $data['pushType'] = 'live'; //直播
        foreach($clients as $fd) {
            $serv->push($fd, json_encode($data));
        }
    }
    /**
     * 将聊天的信息推送给客户端
     * @param $data
     * @param $serv swoole server对象
     */
    public function pushChar($data, $serv) {
//        $clients = Predis::getInstance()->sMembers(config("redis.live_game_key"));
        $data['pushType'] = 'chart'; //聊天
//        foreach($clients as $fd) {
//            $serv->push($fd, json_encode($data));
//        }
        //上面的方式是聊天与直播都是8811端口
        //如果多个端口获取客户端连接数，需要加posts，按照监听端口的顺序
        foreach($serv->ports[1]->connections AS $fd){
            $serv->push($fd, json_encode($data));
        }
    }
}